<?php

namespace Database\Seeders;

use App\Models\Thing;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ThingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Thing::create([
            'id' => '1',
            'name' => 'Sabun batang',
            'price' => '3000',
        ]);

        Thing::create([
            'id' => '2',
            'name' => 'Mie instan',
            'price' => '2000',
        ]);

        Thing::create([
            'id' => '3',
            'name' => 'Pensil',
            'price' => '1000',
        ]);

        Thing::create([
            'id' => '4',
            'name' => 'Kopi sachet',
            'price' => '1500',
        ]);

        Thing::create([
            'id' => '5',
            'name' => 'Air Minum Galon',
            'price' => '20000',
        ]);
    }
}
