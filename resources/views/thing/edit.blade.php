@extends('layouts.master')
@section('title')
    Buat Barang Baru
@endsection

@section('content')
    <form action="{{ route('thing.update', $thing->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="font-weight-bold">Nama Barang</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                value="{{ old('name', $thing->name) }}" placeholder="Masukkan Nama Barang">

            <!-- error message untuk name -->
            @error('name')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label class="font-weight-bold">Harga Barang</label>
            <input type="text" class="form-control @error('price') is-invalid @enderror" name="price" id="price"
                value="{{ old('price', $thing->price) }}" placeholder="Masukkan harga barang">

            <!-- error message untuk price -->
            @error('price')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <button type="reset" class="btn btn-md btn-warning">Reset</button>

    </form>
@endsection
