@extends('layouts.master')

@section('title')
    Barang
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{ route('thing.create') }}" class="btn btn-success btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Tambah Data Barang </span>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th>Harga Barang</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Pengaturan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($things as $thing)
                            <tr>
                                <td>{{ $thing->name }}</td>
                                <td>{{ $thing->price }}</td>
                                <td>{{ $thing->created_at }}</td>
                                <td>{{ $thing->updated_at }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('thing.destroy', $thing->id) }}" method="POST">
                                        <a href="{{ route('thing.edit', $thing->id) }}"
                                            class="btn btn-sm btn-primary">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" align="center">No Data Recorded</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
