@extends('layouts.master')
@section('title')
    Buat Barang Baru
@endsection

@section('content')
    <form action="{{ route('invoice.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="font-weight-bold">Nama Pembeli</label>
            <input type="text" class="form-control @error('buyer') is-invalid @enderror" name="buyer" id="buyer"
                value="{{ old('buyer') }}" placeholder="Masukkan nama Nama Barang">

            <!-- error message untuk buyer -->
            @error('buyer')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <button type="reset" class="btn btn-md btn-warning">Reset</button>

    </form>
@endsection
