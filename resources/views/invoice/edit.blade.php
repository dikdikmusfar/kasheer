@extends('layouts.master')
@section('title')
    Perbaharui Invoice
@endsection

@section('content')
    <form action="{{ route('invoice.update', $invoice->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="font-weight-bold">Nama Pembeli</label>
            <input type="text" class="form-control @error('buyer') is-invalid @enderror" name="buyer" id="buyer"
                value="{{ old('buyer', $invoice->buyer) }}" placeholder="Masukkan nama Pembeli">

            <!-- error message untuk buyer -->
            @error('buyer')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <button type="reset" class="btn btn-md btn-warning">Reset</button>

    </form>
@endsection
