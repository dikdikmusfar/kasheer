@extends('layouts.master')
@section('title')
    Buat Transaksi Baru
@endsection

@section('content')
    <form action="{{ route('transaction.update', $transaction->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="form-label" for="invoice_id">Nama Pembeli</label>
            <select class="form-control" id="invoice_id" name="invoice_id">
                @foreach ($invoices as $invoice)
                    <option value="{{ $invoice->id }}">{{ $invoice->buyer }}</option>
                @endforeach
            </select>
            <!-- error message untuk invoice_id -->
            @error('invoice_id')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label class="form-label" for="thing_id">Nama Barang</label>
            <select class="form-control" id="thing_id" name="thing_id">
                @foreach ($things as $thing)
                    <option value="{{ $thing->id }}">{{ $thing->name }}</option>
                @endforeach
            </select>
            <!-- error message untuk thing_id -->
            @error('thing_id')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label class="font-weight-bold">Amount</label>
            <input type="number" class="form-control @error('amount') is-invalid @enderror" name="amount"
                value="{{ old('amount', $transaction->amount) }}" placeholder="Masukkan nama PIC customer">

            <!-- error message untuk amount -->
            @error('amount')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label class="form-label" for="price">Harga Barang</label>
            <select class="form-control" id="price" name="price">
                @foreach ($things as $thing)
                    <option value="{{ $thing->price }}">{{ $thing->name }}</option>
                @endforeach
            </select>
            <!-- error message untuk price -->
            @error('price')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <button type="reset" class="btn btn-md btn-warning">Reset</button>

    </form>
@endsection
