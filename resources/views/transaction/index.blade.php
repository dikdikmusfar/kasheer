@extends('layouts.master')

@section('title')
    Transaction
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{ route('transaction.create') }}" class="btn btn-success btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Tambah Data Transaksi</span>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Pembeli</th>
                            <th>Nama Barang</th>
                            <th>Amount</th>
                            <th>Price</th>
                            <th>Grand Total</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Pengaturan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->invoice->buyer }}</td>
                                <td>{{ $transaction->thing->name }}</td>
                                <td>{{ $transaction->amount }}</td>
                                <td>{{ $transaction->price }}</td>
                                <td>{{ $transaction->grand }}</td>
                                <td>{{ $transaction->created_at }}</td>
                                <td>{{ $transaction->updated_at }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('transaction.destroy', $transaction->id) }}" method="POST">
                                        <a href="{{ route('transaction.edit', $transaction->id) }}"
                                            class="btn btn-sm btn-primary">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" align="center">No Data Recorded</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
