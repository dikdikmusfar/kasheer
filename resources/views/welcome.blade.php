@extends('layouts.master')

@section('title')
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Tahap awal</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Membuat data barang</div>
                        </div>
                        <div class="col-auto">
                            <a href="/thing"><i class="fas fa-calendar fa-2x text-gray-300"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Annual) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Tahap kedua</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Membuat data pembeli</div>
                        </div>
                        <div class="col-auto">
                            <a href="/invoice"><i class="fas fa-dollar-sign fa-2x text-gray-300"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Tasks Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tahap ketiga
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Membuat transaksi</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <a href="/transaction">
                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Info</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Developer</div>
                        </div>
                        <div class="col-auto">
                            <a href="https://www.instagram.com/dikdikmusfar" target=__blank><i
                                    class="fas fa-comments fa-2x text-gray-300"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card shadow">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">ERD Kasir</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    <img src="{{ asset('img/ERD.png') }}" class="img-fluid" alt="Responsive image">
                </div>
            </div>
        </div>
    </div>
@endsection
