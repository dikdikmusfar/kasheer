<?php

namespace App\Http\Controllers;

use App\Models\Thing;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ThingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $things = Thing::latest()->paginate(false);
        return view('thing.index', compact('things'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('thing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'price'     => 'required',
        ]);

        Thing::create([
            'name' => $request->name,
            'price' => $request->price,
        ]);
        Alert::success('Barang', 'Barang baru telah ditambahkan!');
        return redirect()->route('thing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Thing  $thing
     * @return \Illuminate\Http\Response
     */
    public function show(Thing $thing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Thing  $thing
     * @return \Illuminate\Http\Response
     */
    public function edit(Thing $thing)
    {
        return view('thing.edit', compact('thing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Thing  $thing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thing $thing)
    {
        $this->validate($request, [
            'name'     => 'required',
            'price'   => 'required',
        ]);

        $thing->update([
            'name' => $request->name,
            'price' => $request->price,
        ]);
        Alert::warning('Barang', 'Barang telah diubah!');
        return redirect()->route('thing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Thing  $thing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thing $thing)
    {
        $thing->delete();
        Alert::error('Barang', 'Barang telah dihapus!');
        return redirect()->route('thing.index');
    }
}
