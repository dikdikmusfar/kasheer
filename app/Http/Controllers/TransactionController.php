<?php

namespace App\Http\Controllers;

use App\Models\Thing;
use App\Models\Invoice;
use App\Models\Transaction;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::latest()->paginate(false);
        return view('transaction.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $invoices = Invoice::all();
        $things = Thing::all();
        return view('transaction.create', compact('invoices', 'things'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'invoice_id'     => 'required',
            'thing_id'     => 'required',
            'amount'     => 'required',
            'price'     => 'required',
        ]);

        Transaction::create([
            'invoice_id' => $request->invoice_id,
            'thing_id' => $request->thing_id,
            'amount' => $request->amount,
            'price' => $request->price,
            'grand' => $request->amount * $request->price,
        ]);
        Alert::success('Transaksi', 'Transaksi baru telah ditambahkan!');
        return redirect()->route('transaction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $invoices = Invoice::all();
        $things = Thing::all();
        return view('transaction.edit', compact('transaction', 'invoices', 'things'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $this->validate($request, [
            'invoice_id'     => 'required',
            'thing_id'     => 'required',
            'amount'     => 'required',
            'price'     => 'required',
        ]);

        $transaction->update([
            'invoice_id' => $request->invoice_id,
            'thing_id' => $request->thing_id,
            'amount' => $request->amount,
            'price' => $request->price,
            'grand' => $request->amount * $request->price,
        ]);
        Alert::warning('Transaksi', 'Transaksi telah diubah!');
        return redirect()->route('transaction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        Alert::error('Transaksi', 'Transaksi telah dihapus!');
        return redirect()->route('transaction.index');
    }
}
