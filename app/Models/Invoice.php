<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'buyer',
        'total'
    ];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}
