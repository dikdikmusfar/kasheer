<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id',
        'thing_id',
        'amount',
        'price',
        'grand',
    ];

    public function thing()
    {
        return $this->belongsTo(Thing::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
